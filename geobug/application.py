# Copyright 2021-2024 Teemu Ikonen
# SPDX-License-Identifier: GPL-3.0-only

import argparse
import importlib.resources as resources
import os
import re
import signal
import sys
import time
import tokenize
from datetime import datetime, timezone
from geobug import __version__

import gi
import gpxpy

from .util import bearing_to_arrow, have_touchscreen, unique_filename
from .widgets import DataFrame

gi.require_version('Geoclue', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Handy', '1')
from gi.repository import GLib, Gdk, Geoclue, Gio, Gtk, Handy  # noqa: E402, I100

appname = 'Geobug'
app_id = 'page.codeberg.tpikonen.geobug'

acc2text = {
    Geoclue.AccuracyLevel.COUNTRY: "Country",
    Geoclue.AccuracyLevel.CITY: "City",
    Geoclue.AccuracyLevel.NEIGHBORHOOD: "Neighborhood",
    Geoclue.AccuracyLevel.STREET: "Street",
    Geoclue.AccuracyLevel.EXACT: "Exact",
}

param2acc = {
    "ACC_COUNTRY": Geoclue.AccuracyLevel.COUNTRY,
    "ACC_CITY": Geoclue.AccuracyLevel.CITY,
    "ACC_NEIGHBORHOOD": Geoclue.AccuracyLevel.NEIGHBORHOOD,
    "ACC_STREET": Geoclue.AccuracyLevel.STREET,
    "ACC_EXACT": Geoclue.AccuracyLevel.EXACT,
}

acc2param = {v: k for k, v in param2acc.items()}

default_accuracy = Geoclue.AccuracyLevel.STREET


class GeobugApp(Gtk.Application):

    def __init__(self, *args, **kwargs):
        Gtk.Application.__init__(
            self, *args, application_id=app_id,
            flags=Gio.ApplicationFlags.FLAGS_NONE, **kwargs)
        Handy.init()

        desc = "Adaptive GeoClue client"
        parser = argparse.ArgumentParser(description=desc)
        parser.add_argument(
            '-c', '--console-output', dest='console_output',
            action='store_true', default=False,
            help='Output location data to console')
        parser.add_argument(
            '-a', '--accuracy-level', dest='accuracy',
            choices=['country', 'city', 'neighborhood', 'street', 'exact'],
            default=acc2text[default_accuracy].lower(),
            help="Set initial accuracy level.")
        self.args = parser.parse_args()

        GLib.unix_signal_add(GLib.PRIORITY_DEFAULT, signal.SIGINT,
                             self.sigint_handler)

        self.builder = Gtk.Builder()
        self.builder.add_from_string(
            resources.read_text("geobug", "geobug.ui"))
        self.builder.add_from_string(
            resources.read_text("geobug", "menus.ui"))
        self.builder.connect_signals(self)

        for widget in self.builder.get_objects():
            if not isinstance(widget, Gtk.Buildable):
                continue
            widget_name = Gtk.Buildable.get_name(widget)

            widget_api_name = '_'.join(re.findall(tokenize.Name, widget_name))
            if hasattr(self, widget_api_name):
                raise AttributeError(
                    "instance %s already has an attribute %s" % (
                        self, widget_api_name))
            else:
                setattr(self, widget_api_name, widget)

        self.app_menu = self.builder.get_object('app-menu')
        self.menu_popover = Gtk.Popover.new_from_model(
            self.app_menu_button,
            self.app_menu)
        self.menu_popover.set_position(Gtk.PositionType.BOTTOM)

        optacc2acc = {v.lower(): k for k, v in acc2text.items()}
        self.accuracy = optacc2acc[self.args.accuracy]
        self.accuracy_popover = Gtk.Popover.new_from_model(
            self.accuracy_menubutton,
            self.builder.get_object('accuracy-menu'))
        self.accuracy_menubutton.set_popover(self.accuracy_popover)
        self.relabel_accuracy_menubutton(None, None)

        self.dataframe = DataFrame()
        self.dataframe.header.set_visible(False)
        # Initialize dataframe
        self.set_values({}, console_output=self.args.console_output)
        self.dataframe.show()
        self.main_box.add(self.dataframe)
        self.main_box.set_sensitive(False)

        self.set_speedlabel(None)
        self.leaflet.set_visible_child(self.databox)

        self.connect("startup", self.on_startup)
        self.connect("activate", self.on_activate)
        self.connect("shutdown", self.on_shutdown)

        # Internal state
        self.last_update = None
        self.source = None
        self.source_lost = False
        self.sigint_received = False
        self.refresh_rate = 1  # Really delay between updates in seconds
        self.location = None
        self.prev_data = None

        # GPX
        docdir = (GLib.get_user_special_dir(
            GLib.UserDirectory.DIRECTORY_DOCUMENTS)
            or os.path.join(GLib.get_home_dir(), 'Documents'))
        self.gpx_save_dir = os.path.join(docdir, 'geobug-tracks')
        self.gpx_autosave_points = 20
        self.gpxfile = None
        self.gpx = None
        self.gpx_segment = None
        self.gpx_counter = 0

        self.inhibit_cookie = 0

    def create_actions(self):
        app_actions = (
            ('menu', self.on_menu),
            ('about', self.on_about),
            ('record', self.on_record),
        )
        for aname, cb in app_actions:
            action = Gio.SimpleAction.new(aname, None)
            action.connect('activate', cb)
            self.add_action(action)

        action = Gio.SimpleAction.new_stateful(
            'accuracy', GLib.VariantType.new('s'),
            GLib.Variant.new_string(
                acc2param.get(self.accuracy, default_accuracy)))
        action.connect('activate', self.on_accuracy_changed)
        action.connect('activate', self.relabel_accuracy_menubutton)
        self.add_action(action)

    def setup_styles(self):
        provider = Gtk.CssProvider()
        provider.load_from_data(
            resources.read_binary("geobug", "main.css"))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def on_startup(self, app):
        self.create_actions()
        self.log_msg(f"{appname} version {__version__} started")
        GLib.idle_add(self.init_source)

    def on_activate(self, app):
        self.setup_styles()
        self.add_window(self.window)
        self.window.show()
        if have_touchscreen():
            self.datascroll.connect('edge-overshot', self.on_edge_overshot)

    def on_shutdown(self, app):
        print("Cleaning up...")
        self.gpx_write()
        print("...done.")

    def on_location_notify(self, location, params, *data):
        self.on_location_changed(location)

    def on_location_proxy_ready(self, source, res, *data):
        try:
            location = Geoclue.LocationProxy.new_for_bus_finish(res)
        except Exception:
            self.log_msg("Connection to GeoClue lost")
            return

        self.location = location
        location.connect("notify::timestamp", self.on_location_notify)
        self.on_location_changed(location)

    def on_location_updated(self, client, old_location, new_location, *data):
        Geoclue.LocationProxy.new_for_bus(Gio.BusType.SYSTEM,
                                          Gio.DBusProxyFlags.NONE,
                                          "org.freedesktop.GeoClue2",
                                          new_location,
                                          None,
                                          self.on_location_proxy_ready,
                                          None)

    def on_client_active_changed(self, client, pspec, *data):
        if client.props.active:
            self.log_msg("GeoClue client is active")
        else:
            self.log_msg("Client was stopped by GeoClue")

    def on_client_start_ready(self, source, res, *data):
        Geoclue.ClientProxy.call_start_finish(self.source, res)

    def on_client_proxy_ready(self, source, res, *data):
        try:
            self.source = Geoclue.ClientProxy.create_finish(res)
            if self.source is None:
                raise Exception("ClientProxy.create returned None")
        except Exception as e:
            estr = str(e)
            dtext = estr if estr else ("Could not connect to GeoClue")
            self.log_msg(f"Failed to connect to GeoClue: {dtext}")
            dialog = Gtk.MessageDialog(
                parent=self.window, modal=True,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.OK, text=dtext)
            dialog.set_title("GeoClue not found")
            dialog.run()
            dialog.destroy()
            self.quit()
            return

        if self.source.props.requested_accuracy_level != self.accuracy.real:
            # We have changed accuracy while waiting
            self.source.props.requested_accuracy_level = self.accuracy.real

        self.source.connect("location-updated", self.on_location_updated)
        self.source.connect("notify::active", self.on_client_active_changed)
        self.source.call_start(None, self.on_client_start_ready)
        GLib.timeout_add(self.refresh_rate * 1000, self.timeout_cb, None)
        self.main_box.set_sensitive(True)
        self.log_msg("Connected to GeoClue")

    def init_source(self):
        self.log_msg("Connecting to GeoClue with accuracy '%s' ..." % (
                     acc2text[self.accuracy]))
        Geoclue.ClientProxy.create(app_id,
                                   self.accuracy,
                                   None,
                                   self.on_client_proxy_ready)
        return False  # Remove from idle_add

    def sigint_handler(self):
        if not self.sigint_received:
            print("Interrupt signal (Ctrl-C) received")
            self.sigint_received = True
            self.quit()
        else:
            print("Interrupt signal (Ctrl-C) received again, force exit")
            sys.exit(0)

    def on_menu(self, action, param):
        self.menu_popover.popup()

    def on_edge_overshot(self, scrolledwindow, pos):
        if pos == Gtk.PositionType.TOP:
            self.menu_popover.popup()

    def on_about(self, *args):
        adlg = Gtk.AboutDialog(
            transient_for=self.window,
            modal=True,
            program_name=appname,
            logo_icon_name=app_id,
            version=__version__,
            comments="An adaptive GeoClue client",
            license_type=Gtk.License.GPL_3_0_ONLY,
            copyright="Copyright 2021-2024 Teemu Ikonen",
        )
        adlg.present()

    def on_record(self, *args):
        assert self.gpxfile is None

        namestem = self.gpx_save_dir + '/geobug'
        self.gpxfile = unique_filename(namestem, '.gpx', timestamp=True)
        if self.gpxfile is None:
            raise FileExistsError(namestem)

        os.makedirs(os.path.dirname(self.gpxfile), exist_ok=True)

        # TODO: In-app notification for renaming the track

        gpx = gpxpy.gpx.GPX()

        gpx_track = gpxpy.gpx.GPXTrack()
        gpx.tracks.append(gpx_track)

        gpx_segment = gpxpy.gpx.GPXTrackSegment()
        gpx_track.segments.append(gpx_segment)

        self.gpx = gpx
        self.gpx_segment = gpx_segment

        self.gpxcounter = self.gpx_autosave_points - 5
        self.record_revealer.set_reveal_child(True)
        self.lookup_action('record').set_enabled(False)
        self.gpx_write()
        self.log_msg("Started saving track to '%s'" % self.gpxfile)
        self.inhibit_cookie = self.inhibit(
            self.window, Gtk.ApplicationInhibitFlags.SUSPEND,
            "Recording GPX track")
        if self.inhibit_cookie == 0:
            self.log_msg("Failed to inhibit system suspend")
        else:
            self.log_msg("Inhibiting system suspend")

    def on_stoprecord_button_clicked(self, *args):
        dialog = Gtk.MessageDialog(
            transient_for=self.window,
            flags=0,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            text="Stop recording?",
            secondary_text="Do you want stop recording a track?"
        )
        response = dialog.run()
        if response == Gtk.ResponseType.YES:
            self.gpx_write()
            self.log_msg("Track closed and saved to '%s'" % self.gpxfile)
            self.uninhibit(self.inhibit_cookie)
            self.gpx = None
            self.gpxfile = None
            self.gpx_segment = None
            self.gpx_counter = 0
            self.record_revealer.set_reveal_child(False)
            self.lookup_action('record').set_enabled(True)
            # TODO: In-app notification for renaming the track
        elif response == Gtk.ResponseType.NO:
            pass

        dialog.destroy()

    def leaflet_forward_cb(self, button):
        self.leaflet.navigate(Handy.NavigationDirection.FORWARD)
        return True

    def leaflet_back_cb(self, button):
        self.leaflet.navigate(Handy.NavigationDirection.BACK)
        return True

    def on_accuracy_changed(self, action, param):
        new_accuracy = param2acc.get(param.get_string(),
                                     default_accuracy)
        if new_accuracy == self.accuracy:
            return

        self.accuracy = new_accuracy
        self.log_msg(f"Setting accuracy to {acc2text[self.accuracy]}")
        if self.source is not None:
            self.source.call_stop()
            self.source.props.requested_accuracy_level = self.accuracy.real
            self.source.call_start()
        action.set_state(param)

    def relabel_accuracy_menubutton(self, x_action, x_param):
        label = self.accuracy_menubutton.get_child()
        label.set_text(acc2text[self.accuracy])
        self.accuracy_popover.popdown()

    def set_values(self, data, console_output=True):
        utcfmt = "%H:%M:%S UTC"

        def to_str(x, fmt="%s"):
            return fmt % x if x is not None else "-"

        utcfmt = "%H:%M:%S UTC"
        # Mapping: Data key, description, converter func
        order = [
            ("fixage", "Age of fix", lambda x: to_str(x, "%0.0f s")),
            ("accuracy", "Accuracy", lambda x: to_str(x, "%0.0f m")),
            ("latitude", "Latitude", lambda x: "%0.6f" % x if x else "-"),
            ("longitude", "Longitude", lambda x: "%0.6f" % x if x else "-"),
            ("altitude", "Altitude", lambda x: to_str(x, "%0.1f m")),
            ("speed", "Speed", lambda x: to_str(x, "%0.1f m/s")),
            ("heading", "Heading", lambda x: to_str(x, "%0.1f deg ")
                + (bearing_to_arrow(x) if x is not None else "")),
            ("fixtime", "Time of fix", lambda x: x.strftime(utcfmt)),
            ("systime", "Sys. Time", lambda x: x.strftime(utcfmt)),
            ("description", "Description", lambda x: x if x else "-"),
        ]
        descs = []
        vals = []
        for key, desc, fun in order:
            if key not in data.keys():
                value = "n/a"
            else:
                value = fun(data[key])
            descs.append(desc)
            vals.append(value)

        if self.dataframe.rows != len(descs):
            self.dataframe.set_rowtitles(descs)
        self.dataframe.set_values(vals)

        if console_output:
            for i, val in enumerate(vals):
                print(f"{descs[i]}: {val}")
            print("--")

    def set_speedlabel(self, speed, bearing=None):
        spd = str(int(3.6 * speed)) if speed else "-"
        arrow = bearing_to_arrow(bearing) if bearing is not None else ""
        speedfmt = ('<span size="50000">%s%s</span>\n<span size="30000">%s</span>')
        speedstr = speedfmt % (spd, arrow, "km/h")
        self.speedlabel.set_markup(speedstr)

    def gpx_write(self):
        if not (self.gpx and self.gpxfile):
            return
        with open(self.gpxfile, 'w') as fp:
            fp.write(self.gpx.to_xml('1.0'))  # v1.0 has speed-tag

    def gpx_update(self, data):
        lat = data.get("latitude")
        lon = data.get("longitude")
        if not (lat and lon and self.gpx and self.gpx_segment):
            return

        self.gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(
            lat, lon,
            elevation=data.get("altitude"),
            time=data.get("fixtime"),
            speed=data.get("speed")))
        self.gpx_counter += 1
        if self.gpx_counter > self.gpx_autosave_points:
            self.gpx_write()
            self.gpx_counter = 0
            self.log_msg("Auto-saving track")

    def log_msg(self, text):
        maxlines = 100  # Maximum num of lines in GtkLabel
        msg = datetime.now().strftime("[%H:%M:%S] ") + text
        text = self.loglabel.get_text().split('\n')
        if len(text) > maxlines:
            text = text[1:]
        text.append(msg)
        self.loglabel.set_text("\n".join(text))
        print(msg)

    def timeout_cb(self, x):
        data = self.prev_data
        if not data:
            return True

        now_dt = datetime.now(timezone.utc)
        data['systime'] = now_dt
        data['fixage'] = (now_dt - data['fixtime']).total_seconds()

        dt = ((time.time() - self.last_update)
              if self.last_update else 3 * self.refresh_rate)
        if dt > self.refresh_rate:
            self.set_values(data, console_output=False)
        return True

    def on_location_changed(self, location, *args):
        self.last_update = time.time()
        self.update(location)

    def update(self, new_location):
        if self.source is None:
            return

        if self.source_lost:
            self.log_msg("Recovered")
            self.main_box.set_sensitive(True)

        keys = ('accuracy', 'altitude', 'description', 'heading', 'latitude',
                'longitude', 'speed', 'timestamp')
        data = {k: getattr(new_location.props, k) for k in keys
                if getattr(new_location.props, k, None) is not None}

        if data.get('altitude', -1.0e6) < -1.0e5:
            data["altitude"] = None

        if data.get('speed', -1.0) < 0.0:
            data["speed"] = None

        if data.get('heading', -1.0) < 0.0:
            data["heading"] = None

        now_dt = datetime.now(timezone.utc)
        data['systime'] = now_dt

        def variant2dt(x, default=None):
            if not isinstance(x, GLib.Variant):
                return default
            sec, usec = x.unpack()
            ts = float(sec) + 1.0e-6 * float(usec)
            return datetime.fromtimestamp(ts, timezone.utc)

        fixtime = variant2dt(
            data['timestamp'],
            self.prev_data.get('fixtime') if self.prev_data else None)
        if fixtime is not None:
            data['fixage'] = (now_dt - fixtime).total_seconds()
        else:
            data['fixage'] = None
        data['fixtime'] = fixtime

        self.set_values(data, console_output=self.args.console_output)
        speed = data['speed']
        bearing = data['heading']
        if new_location:
            self.set_speedlabel(speed, bearing)
            if self.gpx is not None:
                self.gpx_update(data)

        self.prev_data = data

        return True
