# Copyright 2021-2024 Teemu Ikonen
# SPDX-License-Identifier: GPL-3.0-only

import sys

from .application import GeobugApp


def main():
    app = GeobugApp()
    app.run()
    sys.exit(0)


if __name__ == '__main__':
    main()
